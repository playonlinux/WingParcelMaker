import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { DropModule } from '../src/drop/drop.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [DropModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('Drop test suite', () => {

    it('should return ', () => {
      return request(app.getHttpServer())
        .get('/drop/make')
        .expect(200)
        .expect((response) => expect(response.body).toMatchObject(result));
    });
  });

});


const result = {
  "parcels": [
    {
      "order_id": "5bb61dfd3741808151aa413b",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 4
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        },
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 28.9,
      // "tracking_id": "106518431",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd3741808151aa413b",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 26.799999999999997,
      // "tracking_id": "109702074",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd3741808151aa413b",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102540074",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd3741808151aa413b",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100544956",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd3741808151aa413b",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "100319564",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd608be72f6dc188b1",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 26.299999999999997,
      // "tracking_id": "109044253",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd608be72f6dc188b1",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105411523",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd608be72f6dc188b1",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105322992",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd608be72f6dc188b1",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106815528",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd608be72f6dc188b1",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106577035",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd608be72f6dc188b1",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "109301574",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd608be72f6dc188b1",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "109157754",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd608be72f6dc188b1",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101378169",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfd608be72f6dc188b1",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "107782916",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfda0ba9350ed572939",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 22.4,
      // "tracking_id": "109584680",
      "palette_number": 1
    },
    {
      "order_id": "5bb61dfda0ba9350ed572939",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105761974",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfda0ba9350ed572939",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107480758",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfda0ba9350ed572939",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102730075",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfda0ba9350ed572939",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101817229",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfda0ba9350ed572939",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "104174779",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfda0ba9350ed572939",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101054739",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfda0ba9350ed572939",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101717460",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfda0ba9350ed572939",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103252650",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfdf343a51e8519467e",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 29.700000000000003,
      // "tracking_id": "101449323",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfdf343a51e8519467e",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 26.299999999999997,
      // "tracking_id": "106583005",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfdf343a51e8519467e",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "100806228",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfdf343a51e8519467e",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        }
      ],
      "weight": 23.6,
      // "tracking_id": "101765785",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfdf343a51e8519467e",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "108763597",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107839265",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103883385",
      "palette_number": 2
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108838305",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107601682",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "109740858",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105191996",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101645814",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101782546",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107279273",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "103359909",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108190497",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104650090",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109664898",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd353280380a766d32",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103557709",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd7a54c2a7fb1b008a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 29.200000000000003,
      // "tracking_id": "102216750",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd7a54c2a7fb1b008a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106854352",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd7a54c2a7fb1b008a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101995861",
      "palette_number": 3
    },
    {
      "order_id": "5bb61dfd7a54c2a7fb1b008a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107886590",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd7a54c2a7fb1b008a",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104671143",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd7a54c2a7fb1b008a",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107408688",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd7a54c2a7fb1b008a",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "109739937",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd958dde6f77377c11",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105618690",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd958dde6f77377c11",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104018329",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd958dde6f77377c11",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101412042",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104006871",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105809266",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106263220",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100039299",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101710399",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        }
      ],
      "weight": 23.6,
      // "tracking_id": "100089570",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104395071",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106823152",
      "palette_number": 4
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "100445697",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103143153",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105639050",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100337846",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106107076",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd1cf4a3dcd5bebb36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107412471",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfda60406879c1de883",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 8.4,
      // "tracking_id": "109455234",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd4060fd0a3c288813",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 27.2,
      // "tracking_id": "107737153",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd4060fd0a3c288813",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106987636",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd4060fd0a3c288813",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101602927",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd4060fd0a3c288813",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103924110",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd4060fd0a3c288813",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103964382",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd4060fd0a3c288813",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        }
      ],
      "weight": 11.8,
      // "tracking_id": "102380510",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd4060fd0a3c288813",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107726009",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd4060fd0a3c288813",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101136958",
      "palette_number": 5
    },
    {
      "order_id": "5bb61dfd4060fd0a3c288813",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104329985",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfd951fae1b928148b0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106370588",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfd951fae1b928148b0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107895613",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfd951fae1b928148b0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101203331",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfdc88b3200af75befb",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        },
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 25.7,
      // "tracking_id": "106757724",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfdc88b3200af75befb",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 22.9,
      // "tracking_id": "104487603",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfdc88b3200af75befb",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103714541",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfdc88b3200af75befb",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102316017",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 26.299999999999997,
      // "tracking_id": "107458762",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107574523",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102883141",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101971723",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106520467",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106129974",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "104028593",
      "palette_number": 6
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108016110",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102556898",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "107794894",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfde99f5fc4596b2248",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "103394215",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd3d8ce14ed6f76ce1",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "103411404",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd3d8ce14ed6f76ce1",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103499153",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd3d8ce14ed6f76ce1",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100871680",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd3d8ce14ed6f76ce1",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109977116",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd7c4e349270d66525",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 28.6,
      // "tracking_id": "108749821",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd7c4e349270d66525",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107234750",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd7c4e349270d66525",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108805473",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd7c4e349270d66525",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103548304",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd7c4e349270d66525",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104643084",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd7c4e349270d66525",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101265037",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd7c4e349270d66525",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106867549",
      "palette_number": 7
    },
    {
      "order_id": "5bb61dfd7c4e349270d66525",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "104937699",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd7c4e349270d66525",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108421776",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 19.200000000000003,
      // "tracking_id": "100702242",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102134176",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100322247",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "105052958",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104754847",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "103307687",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103965200",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108846323",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106332046",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106078805",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103215373",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109323905",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102460451",
      "palette_number": 8
    },
    {
      "order_id": "5bb61dfd1c520e317a9b6377",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100655100",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4c3d2b68f0ee3306",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "103908400",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4c3d2b68f0ee3306",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105146154",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4c3d2b68f0ee3306",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 26.299999999999997,
      // "tracking_id": "106236014",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4c3d2b68f0ee3306",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108798001",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfdaee4269ccaff5f2d",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        }
      ],
      "weight": 20.200000000000003,
      // "tracking_id": "104579041",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd059806b0319699f7",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "107616018",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 25.3,
      // "tracking_id": "101393491",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107844675",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103264731",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103935920",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101718262",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107144591",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "100454680",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "109872108",
      "palette_number": 9
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102607238",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107478220",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106201633",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109383407",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105100120",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106075161",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd4bcfa9098d88386a",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108896188",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 25.799999999999997,
      // "tracking_id": "106098588",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108888522",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106555118",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "107135202",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "104553231",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105493715",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103112399",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109482420",
      "palette_number": 10
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107237903",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfd0cbe9751ea3b5c25",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "107292363",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfdc914531d3eb310c6",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 22.3,
      // "tracking_id": "102150196",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfdc914531d3eb310c6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108681144",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfdc914531d3eb310c6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109631759",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfdc914531d3eb310c6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102363985",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfdc914531d3eb310c6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103778034",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfdc914531d3eb310c6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101970863",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfdc914531d3eb310c6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109621473",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfda0761a938c0a53e2",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 22.4,
      // "tracking_id": "104558376",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfd29107d89c6bb5404",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 24.2,
      // "tracking_id": "109417313",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfd29107d89c6bb5404",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "101887491",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfd29107d89c6bb5404",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 4
        }
      ],
      "weight": 14.4,
      // "tracking_id": "109999433",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfd65d5426c6f7e96e6",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 4
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 28.4,
      // "tracking_id": "105433754",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfd65d5426c6f7e96e6",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 23.799999999999997,
      // "tracking_id": "108978052",
      "palette_number": 11
    },
    {
      "order_id": "5bb61dfd65d5426c6f7e96e6",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100613719",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd65d5426c6f7e96e6",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107283258",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 22.3,
      // "tracking_id": "104728338",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "100978328",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107165134",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104264596",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107191788",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104118718",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103381792",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109356680",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "106538132",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103777690",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100477186",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfd50d9afefef3c00fb",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108017485",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 26.299999999999997,
      // "tracking_id": "108780150",
      "palette_number": 12
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102218138",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103253939",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102479041",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100537494",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102553377",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102004575",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109870912",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103848168",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105870586",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101853075",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "107899864",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdb5050d7fb7847201",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "107295245",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdd8b0a1f90066734d",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102841016",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdd8b0a1f90066734d",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109318704",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdd8b0a1f90066734d",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107071085",
      "palette_number": 13
    },
    {
      "order_id": "5bb61dfdd8b0a1f90066734d",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103019095",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104161342",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105885451",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "109444944",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "109128516",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "103157912",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101429568",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100557391",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100694136",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105502693",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "102705807",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100543465",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101975491",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104886820",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101693651",
      "palette_number": 14
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101311765",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101928375",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfdb0d60749c11d0f74",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102437148",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd785590852ac3dc5d",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 8.4,
      // "tracking_id": "106004263",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd45de57c146626327",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        },
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 27.2,
      // "tracking_id": "100416956",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd45de57c146626327",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106624033",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd45de57c146626327",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107756231",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd45de57c146626327",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101754430",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd45de57c146626327",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106939937",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd5e3fec29772e83a4",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 26.299999999999997,
      // "tracking_id": "101049121",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd5e3fec29772e83a4",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 26.299999999999997,
      // "tracking_id": "104490220",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd5e3fec29772e83a4",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109989497",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd5e3fec29772e83a4",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102506775",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd5e3fec29772e83a4",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "106924656",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfd5e3fec29772e83a4",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "106269715",
      "palette_number": 15
    },
    {
      "order_id": "5bb61dfdd5fee045a9b1fcba",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        }
      ],
      "weight": 21.4,
      // "tracking_id": "100206751",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdd5fee045a9b1fcba",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108317623",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdd5fee045a9b1fcba",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        }
      ],
      "weight": 29.500000000000004,
      // "tracking_id": "106823670",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdd5fee045a9b1fcba",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107826849",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdd5fee045a9b1fcba",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104949986",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdb50589527271d312",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 8.4,
      // "tracking_id": "107763347",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104333297",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "100184484",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106262633",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108210478",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103948449",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103195599",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104907469",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106675327",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103104749",
      "palette_number": 16
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101505464",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "102683038",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfdf5f02f7a4fca9fdf",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "105119172",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        },
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 23.8,
      // "tracking_id": "105355194",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 29.200000000000003,
      // "tracking_id": "103026935",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 29.7,
      // "tracking_id": "105758326",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104795704",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107645262",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "105705734",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101161639",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "107538549",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101036323",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100428876",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101632482",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106903647",
      "palette_number": 17
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101553150",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd824ba0fdce8be731",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101714489",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 4
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 23.9,
      // "tracking_id": "101872563",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103115692",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        }
      ],
      "weight": 23.6,
      // "tracking_id": "101401645",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        }
      ],
      "weight": 23.6,
      // "tracking_id": "109164925",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103348052",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109642339",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "107182553",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101852656",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "104207335",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "105008481",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd7bbfc34028d0eaaf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100140401",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd9c441b19cc090169",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        }
      ],
      "weight": 20.9,
      // "tracking_id": "105884224",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd9c441b19cc090169",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        }
      ],
      "weight": 29.7,
      // "tracking_id": "109214448",
      "palette_number": 18
    },
    {
      "order_id": "5bb61dfd9c441b19cc090169",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "108298829",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 25.799999999999997,
      // "tracking_id": "101123176",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103857832",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104317793",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104070201",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103253684",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106170061",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "105783863",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109660134",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108998096",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102088333",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfde3efd55943f3f411",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101649525",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfd08c1294d217e2c1c",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100605490",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfd08c1294d217e2c1c",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "104029592",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfd08c1294d217e2c1c",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "107341970",
      "palette_number": 19
    },
    {
      "order_id": "5bb61dfd08c1294d217e2c1c",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "105283670",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102913913",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106213654",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        }
      ],
      "weight": 23.6,
      // "tracking_id": "101559406",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "108916747",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101516267",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108359885",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107302220",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109729255",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105558876",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd1e4412b9a65f2e4b",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105392331",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd34535aa451c7426d",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        }
      ],
      "weight": 11.8,
      // "tracking_id": "106270898",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd7ba8e0178befa9ae",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104206179",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd7ba8e0178befa9ae",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102351928",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd7ba8e0178befa9ae",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "100077221",
      "palette_number": 20
    },
    {
      "order_id": "5bb61dfd7ba8e0178befa9ae",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104769121",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd7ba8e0178befa9ae",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "100594493",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd7ba8e0178befa9ae",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 8.4,
      // "tracking_id": "105445701",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd9e7b7353be4184d2",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 22.3,
      // "tracking_id": "105756483",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd9e7b7353be4184d2",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108483508",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd9e7b7353be4184d2",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106080233",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd9e7b7353be4184d2",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102661896",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd9e7b7353be4184d2",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101738176",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd9e7b7353be4184d2",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109635764",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd9e7b7353be4184d2",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104634714",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd9e7b7353be4184d2",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103870668",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfd9e7b7353be4184d2",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107159459",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfdaaaf8863c9cb07e2",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104204921",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfdaaaf8863c9cb07e2",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104356082",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfdaaaf8863c9cb07e2",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        }
      ],
      "weight": 23.6,
      // "tracking_id": "103818407",
      "palette_number": 21
    },
    {
      "order_id": "5bb61dfdaaaf8863c9cb07e2",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "107371863",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfdaaaf8863c9cb07e2",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106269210",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfdaaaf8863c9cb07e2",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106722311",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfdaaaf8863c9cb07e2",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100823299",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfdaaaf8863c9cb07e2",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108962507",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        }
      ],
      "weight": 29.7,
      // "tracking_id": "109302295",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102100082",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101022335",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102578693",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104816283",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101249900",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "100242087",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104292535",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105036429",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106669760",
      "palette_number": 22
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109683443",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105491673",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfde6420081aa3f8fa0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "103365228",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd7d9320c7ccc53e1f",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102065118",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd7d9320c7ccc53e1f",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103156694",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd7d9320c7ccc53e1f",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105230718",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd7d9320c7ccc53e1f",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102032932",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd7d9320c7ccc53e1f",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108041350",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 26.8,
      // "tracking_id": "107984648",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 26.700000000000003,
      // "tracking_id": "102871058",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105189971",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100563099",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106793584",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108736878",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100907594",
      "palette_number": 23
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107846407",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102962851",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfd0791d8a355e1d2d0",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102960844",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104077084",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102091949",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107487992",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108627000",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100151884",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109932811",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103591127",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108592211",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102007700",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108400919",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105053381",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdbe34a8051c6a74bb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104913706",
      "palette_number": 24
    },
    {
      "order_id": "5bb61dfdd5cb341205e758b4",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 4
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105970243",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfdd5cb341205e758b4",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106768950",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfdd5cb341205e758b4",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "100279538",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd9bb9170e3145ea06",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 28.6,
      // "tracking_id": "102709425",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd9bb9170e3145ea06",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 1.5,
      // "tracking_id": "102480014",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 29.299999999999997,
      // "tracking_id": "105753231",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103337487",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106919444",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "107192787",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107826062",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101004767",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107894453",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101803780",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105185777",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100930211",
      "palette_number": 25
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109474681",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108197207",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd79a85ef574665251",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108457093",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 26.799999999999997,
      // "tracking_id": "100896478",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106348880",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        }
      ],
      "weight": 23.6,
      // "tracking_id": "106072792",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100456722",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104201106",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "104619267",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102580771",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108987218",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100391269",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd048ea2e9f38a67aa",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103109003",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd6dc15646e572b664",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "100689886",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd6dc15646e572b664",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 16.8,
      // "tracking_id": "107269582",
      "palette_number": 26
    },
    {
      "order_id": "5bb61dfd6dc15646e572b664",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "102318213",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd6dc15646e572b664",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109648631",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd6dc15646e572b664",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100270163",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd6dc15646e572b664",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "107743414",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd280a1e8bfafa866e",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 28.6,
      // "tracking_id": "107733236",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd280a1e8bfafa866e",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104016663",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd280a1e8bfafa866e",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106947531",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd280a1e8bfafa866e",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "109982130",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd280a1e8bfafa866e",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105591625",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd280a1e8bfafa866e",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101535509",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd280a1e8bfafa866e",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104975055",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd280a1e8bfafa866e",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102519538",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfd280a1e8bfafa866e",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106706663",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfda8578902208a9291",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 19.4,
      // "tracking_id": "101776090",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfda8578902208a9291",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108665810",
      "palette_number": 27
    },
    {
      "order_id": "5bb61dfda8578902208a9291",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107056819",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfda8578902208a9291",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106201719",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfda8578902208a9291",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106216407",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfda8578902208a9291",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108677583",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfda8578902208a9291",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109908208",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfdd1908c624b0f24ea",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 4
        }
      ],
      "weight": 24.4,
      // "tracking_id": "104415485",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfdd1908c624b0f24ea",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "107789606",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfdd1908c624b0f24ea",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102363065",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfdd1908c624b0f24ea",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106981006",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfdd1908c624b0f24ea",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105865097",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfdd1908c624b0f24ea",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101801885",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfd62d16b505c525bbb",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "102677901",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfd62d16b505c525bbb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107046727",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfd62d16b505c525bbb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109666461",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfd62d16b505c525bbb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104996175",
      "palette_number": 28
    },
    {
      "order_id": "5bb61dfd62d16b505c525bbb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101792835",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfd62d16b505c525bbb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101563642",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfd62d16b505c525bbb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105781975",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfd62d16b505c525bbb",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103118710",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfd62d16b505c525bbb",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "103742094",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfd87895d95ebf1a065",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102720393",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfd87895d95ebf1a065",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100987036",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfd87895d95ebf1a065",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109585315",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfd87895d95ebf1a065",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102612976",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101250238",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102113371",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108448987",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "103759769",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "109962892",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 26.799999999999997,
      // "tracking_id": "109855697",
      "palette_number": 29
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108030636",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106769557",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106196864",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102489638",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfdc72d051fb63589cb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100313883",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfdf8878ffeab69d077",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 29.5,
      // "tracking_id": "107390932",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfdf8878ffeab69d077",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 26.700000000000003,
      // "tracking_id": "100490768",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfdf8878ffeab69d077",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 8.4,
      // "tracking_id": "109321384",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfda907f0aa69903271",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "109745565",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "104177235",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 23.799999999999997,
      // "tracking_id": "106537809",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104560028",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "106836301",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101264640",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107859764",
      "palette_number": 30
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104426487",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105761849",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102475056",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102286294",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109589391",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd35ad7371ae383e36",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109193361",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd2575864ab2e78cf2",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104494897",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd2575864ab2e78cf2",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105724014",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd2575864ab2e78cf2",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109268455",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd2575864ab2e78cf2",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "105838534",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd2575864ab2e78cf2",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103994823",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd2575864ab2e78cf2",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "104537535",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfdb2d11c31eb3cdb2c",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "104467923",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd550fbf4bfdfd3b9f",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        }
      ],
      "weight": 22.4,
      // "tracking_id": "105488446",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd550fbf4bfdfd3b9f",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108285531",
      "palette_number": 31
    },
    {
      "order_id": "5bb61dfd550fbf4bfdfd3b9f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101712183",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfd550fbf4bfdfd3b9f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100966596",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfd550fbf4bfdfd3b9f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100728994",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfd550fbf4bfdfd3b9f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101767468",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfd550fbf4bfdfd3b9f",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        }
      ],
      "weight": 29.500000000000004,
      // "tracking_id": "101063571",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfd55adac4cef0c7587",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 22.4,
      // "tracking_id": "106549010",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfd55adac4cef0c7587",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        }
      ],
      "weight": 29.7,
      // "tracking_id": "100804905",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfd55adac4cef0c7587",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100533751",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfd55adac4cef0c7587",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101412068",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfd37f3d80d39d2eeb1",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "107489315",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfdb6780806abe2160b",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 22.9,
      // "tracking_id": "104122780",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfdb6780806abe2160b",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "101060769",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfdb6780806abe2160b",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "103655534",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfdb6780806abe2160b",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109930251",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfdb6780806abe2160b",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108287197",
      "palette_number": 32
    },
    {
      "order_id": "5bb61dfdb6780806abe2160b",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104611983",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfdb6780806abe2160b",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102411137",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfdb6780806abe2160b",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105919881",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfdb6780806abe2160b",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104328830",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd9bfc575f5b95a4f9",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103898951",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd9bfc575f5b95a4f9",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104393363",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd4c859cd301829650",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 14.4,
      // "tracking_id": "109731666",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd4c859cd301829650",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "109570358",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd6dc24173b2389726",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "108309267",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd6dc24173b2389726",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 8.4,
      // "tracking_id": "107871167",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 21.3,
      // "tracking_id": "100840632",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108405563",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104764934",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105509135",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108810577",
      "palette_number": 33
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "103761096",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "104427114",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107597163",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105572436",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109282810",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102512269",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106793940",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103961274",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101845670",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd341869915fa31540",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101367815",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd5e6124d47ed3e049",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "104570633",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd5e6124d47ed3e049",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104066453",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd5e6124d47ed3e049",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "102157403",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd5e6124d47ed3e049",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105989895",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfd5e6124d47ed3e049",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108008871",
      "palette_number": 34
    },
    {
      "order_id": "5bb61dfdd2c575954d819651",
      "items": [
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 21.4,
      // "tracking_id": "106009502",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd749a93bdbbea4b6a",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 4
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107253235",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd749a93bdbbea4b6a",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104172719",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd749a93bdbbea4b6a",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "103199140",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd749a93bdbbea4b6a",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "100271063",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd749a93bdbbea4b6a",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105881281",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd749a93bdbbea4b6a",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107063326",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd749a93bdbbea4b6a",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104689186",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd749a93bdbbea4b6a",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104249381",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd749a93bdbbea4b6a",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "106864802",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 28.299999999999997,
      // "tracking_id": "101443917",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 26.299999999999997,
      // "tracking_id": "103071038",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108616829",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106211744",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102149841",
      "palette_number": 35
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107466267",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104516825",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102836556",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107463683",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfd8b3a9057af5ca05b",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "106753102",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfda46ae7ee3107cab7",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        }
      ],
      "weight": 26.8,
      // "tracking_id": "104047998",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfda46ae7ee3107cab7",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109928678",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfda46ae7ee3107cab7",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103561961",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfda46ae7ee3107cab7",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101482291",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfda46ae7ee3107cab7",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103813950",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfda46ae7ee3107cab7",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102959777",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfda46ae7ee3107cab7",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100725350",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfda46ae7ee3107cab7",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        }
      ],
      "weight": 23.6,
      // "tracking_id": "107462736",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "105086637",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108148867",
      "palette_number": 36
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109518124",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101346625",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100462184",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102322426",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108989149",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102352532",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108943495",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106272761",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101367897",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd117a321d7bd5007f",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "106123488",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        }
      ],
      "weight": 28.3,
      // "tracking_id": "105369246",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107152007",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107080445",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105230715",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105555234",
      "palette_number": 37
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101109242",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102300813",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108465301",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107811479",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "107874982",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105465823",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd9f066dcdc21126fb",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104587722",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 23.799999999999997,
      // "tracking_id": "104005023",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103163190",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104542808",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109850572",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "102288887",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "108212297",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106080405",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100212589",
      "palette_number": 38
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105886978",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd065632a4a35db675",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108729610",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd8b100945ccda62e6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105040951",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd8b100945ccda62e6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109106867",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd8b100945ccda62e6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100549633",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd8b100945ccda62e6",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104378944",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd8b100945ccda62e6",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102283374",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd563905ccb74f3561",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "100581745",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd563905ccb74f3561",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102461358",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd563905ccb74f3561",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101127784",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd563905ccb74f3561",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104786883",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd563905ccb74f3561",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101559350",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd563905ccb74f3561",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109222880",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd7a7fcf71419047d9",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 29.200000000000003,
      // "tracking_id": "104628506",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd7a7fcf71419047d9",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "101854137",
      "palette_number": 39
    },
    {
      "order_id": "5bb61dfd7a7fcf71419047d9",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107991986",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfd7a7fcf71419047d9",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103004199",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfd7a7fcf71419047d9",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104847861",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfd4f69490e2a0dde02",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 29.700000000000003,
      // "tracking_id": "100817057",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfd4f69490e2a0dde02",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "102473289",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfd4f69490e2a0dde02",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102206254",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfd4f69490e2a0dde02",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108738956",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfd4f69490e2a0dde02",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "105406274",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfd4f69490e2a0dde02",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "108456854",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfd4f69490e2a0dde02",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 4
        }
      ],
      "weight": 23.6,
      // "tracking_id": "106369239",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfdafd60889048c1725",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "103216958",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfdafd60889048c1725",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 29.200000000000003,
      // "tracking_id": "100030562",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfdafd60889048c1725",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 29.200000000000003,
      // "tracking_id": "105830422",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfdafd60889048c1725",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "108606640",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfdafd60889048c1725",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "107769227",
      "palette_number": 40
    },
    {
      "order_id": "5bb61dfdafd60889048c1725",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "109986000",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfdafd60889048c1725",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "106345502",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfd64d86bedb0b74bee",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 4
        }
      ],
      "weight": 24.4,
      // "tracking_id": "106799471",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfd64d86bedb0b74bee",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109519949",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfd64d86bedb0b74bee",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104131902",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfd64d86bedb0b74bee",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108730371",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfd74a462a0f7091462",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102213801",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfd74a462a0f7091462",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102204903",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 3
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 28.7,
      // "tracking_id": "106261717",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105561329",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106694953",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106986732",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100900607",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106986837",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108645267",
      "palette_number": 41
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105381406",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108032208",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105440024",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "103281581",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101600997",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105943069",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108761898",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106643242",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfda2ae8cbbc088ac6f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "100667374",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfd07744d5420f11578",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        }
      ],
      "weight": 22.700000000000003,
      // "tracking_id": "100234999",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfd07744d5420f11578",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108669119",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfd07744d5420f11578",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "108267509",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfd07744d5420f11578",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "105287188",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfdcb54de8b66f8dfab",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        },
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 28.6,
      // "tracking_id": "104762730",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfdcb54de8b66f8dfab",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 29.200000000000003,
      // "tracking_id": "105018895",
      "palette_number": 42
    },
    {
      "order_id": "5bb61dfdcb54de8b66f8dfab",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "100263878",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd871cb0a45592716c",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "102908544",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd871cb0a45592716c",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 29.200000000000003,
      // "tracking_id": "108268477",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd871cb0a45592716c",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106764264",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd871cb0a45592716c",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 2
        }
      ],
      "weight": 16.8,
      // "tracking_id": "108770393",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd871cb0a45592716c",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109659864",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd871cb0a45592716c",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "108902594",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd871cb0a45592716c",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "102171747",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd871cb0a45592716c",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109420914",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd871cb0a45592716c",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "106029514",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103658581",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100558747",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "104549688",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106253710",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104394665",
      "palette_number": 43
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108374586",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109898550",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "103663825",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "109282481",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104018027",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102697465",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "106324853",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfd66abb0ff34411453",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "104324816",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 27.799999999999997,
      // "tracking_id": "100993142",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105287463",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107173668",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101961916",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108790511",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "101977348",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "108973110",
      "palette_number": 44
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100540388",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106302646",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105719159",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "101495541",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104883782",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdfcfd2d8ef52c4657",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102928764",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdce0796e5a72cd45f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 2
        }
      ],
      "weight": 25.7,
      // "tracking_id": "102001694",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdce0796e5a72cd45f",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 4
        }
      ],
      "weight": 28.7,
      // "tracking_id": "109392952",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdce0796e5a72cd45f",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        }
      ],
      "weight": 17.700000000000003,
      // "tracking_id": "109100060",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdce0796e5a72cd45f",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "102891767",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfdce0796e5a72cd45f",
      "items": [
        {
          "item_id": "5bb619e4911037797edae511",
          "quantity": 1
        }
      ],
      "weight": 20.8,
      // "tracking_id": "103539733",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfd8d56e6ddf4cb06c8",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "105563734",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfd8d56e6ddf4cb06c8",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "104186425",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfd8d56e6ddf4cb06c8",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "108278324",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfd8d56e6ddf4cb06c8",
      "items": [
        {
          "item_id": "5bb619e439d3e99e2e25848d",
          "quantity": 1
        }
      ],
      "weight": 22.7,
      // "tracking_id": "102201108",
      "palette_number": 45
    },
    {
      "order_id": "5bb61dfde45aa38a3a9d5be6",
      "items": [
        {
          "item_id": "5bb619e4ebdccb9218aa9dcb",
          "quantity": 3
        }
      ],
      "weight": 25.200000000000003,
      // "tracking_id": "109059854",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd7a317891143f34a3",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 1
        },
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 25.299999999999997,
      // "tracking_id": "101207923",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd7a317891143f34a3",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100192648",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd7a317891143f34a3",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102253730",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd7a317891143f34a3",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "107017763",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd7a317891143f34a3",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "102550645",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd5949626d7e1446bb",
      "items": [
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 3
        },
        {
          "item_id": "5bb619e49593e5d3cbaa0b52",
          "quantity": 1
        }
      ],
      "weight": 19.200000000000003,
      // "tracking_id": "107969263",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd5949626d7e1446bb",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "100168957",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd5949626d7e1446bb",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "103660580",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd5949626d7e1446bb",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101275283",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd4d64747dd8d7d6cf",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        },
        {
          "item_id": "5bb619e4504f248e1be543d3",
          "quantity": 2
        }
      ],
      "weight": 29.7,
      // "tracking_id": "108151027",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd4d64747dd8d7d6cf",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "106573570",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd4d64747dd8d7d6cf",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "100234541",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd4d64747dd8d7d6cf",
      "items": [
        {
          "item_id": "5bb619e44251009d72e458b9",
          "quantity": 1
        }
      ],
      "weight": 17.9,
      // "tracking_id": "105298608",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd4d64747dd8d7d6cf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109238772",
      "palette_number": 46
    },
    {
      "order_id": "5bb61dfd4d64747dd8d7d6cf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "109971634",
      "palette_number": 47
    },
    {
      "order_id": "5bb61dfd4d64747dd8d7d6cf",
      "items": [
        {
          "item_id": "5bb619e40fee29e3aaf09759",
          "quantity": 1
        }
      ],
      "weight": 18.4,
      // "tracking_id": "101749786",
      "palette_number": 47
    }
  ],
  "remuneration": 5285
}