import { Module } from '@nestjs/common';
import { IFORMATTER_TOKEN, IPARSER_TOKEN } from '../interfaces';
import { DropController } from './controllers';
import {
  DropFormatterService,
  DropParserService,
  DropService,
  ItemService,
  OrderService,
} from './services';
import { RemunerationService } from './services/remuneration.service';

@Module({
  imports: [],
  controllers: [DropController],
  providers: [
    DropService,
    RemunerationService,
    OrderService,
    ItemService,
    {
      provide: IPARSER_TOKEN,
      useClass: DropParserService,
    },
    {
      provide: IFORMATTER_TOKEN,
      useClass: DropFormatterService,
    },
  ],
})
export class DropModule { }
