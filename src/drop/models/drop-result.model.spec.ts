import { validate } from "class-validator";
import { DropResult } from "./drop-result.model";
import { OrderItem } from "./order-item.model";
import { Parcel } from "./parcel.model";

describe('DropResult', () => {

  describe('should throw when parcels array or remuneration are missing', () => {
    it('when parcel array is missing', async () => {
      const dropResult = new DropResult(undefined, 3);
      const errors = await validate(dropResult);
      expect(errors.length).not.toBe(0);
    });
    it('when parcel array is empty', async () => {
      const dropResult = new DropResult([], 3);
      const errors = await validate(dropResult);
      expect(errors.length).not.toBe(0);
    });
    it('when remuneration is missing', async () => {
      const dropResult = new DropResult([
        new Parcel('orderId', [new OrderItem('iu', 4)], 4)
      ], undefined);
      const errors = await validate(dropResult);
      expect(errors.length).not.toBe(0);
    });
  });
});

