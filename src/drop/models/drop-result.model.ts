import { ApiProperty } from "@nestjs/swagger";
import { ArrayNotEmpty, IsArray, IsNotEmpty, IsNumber } from "class-validator";
import { Model } from "../../interfaces";
import { Parcel } from "./parcel.model";

export class DropResult extends Model {

  @ApiProperty({ type: [Parcel] })
  @IsArray()
  @ArrayNotEmpty()
  @IsNotEmpty()
  public parcels: Parcel[];


  @ApiProperty({ type: Number })
  @IsNumber()
  @IsNotEmpty()
  public remuneration: number;

  constructor(parcels: Parcel[], remuneration: number) {
    super();
    this.parcels = parcels;
    this.remuneration = remuneration;
  }
}