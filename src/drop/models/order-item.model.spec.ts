import { validate } from "class-validator";
import { OrderItem } from "./order-item.model";

describe('OrderItem', () => {

  describe('should throw when itemId or quantity are missing', () => {
    it('when id is missing', async () => {
      const orderItem = new OrderItem(undefined, 2);
      const errors = await validate(orderItem);
      expect(errors.length).not.toBe(0);
    });
    it('when items array is missing', async () => {
      const orderItem = new OrderItem('itemId', undefined);
      const errors = await validate(orderItem);
      expect(errors.length).not.toBe(0);
    });
  });
});

