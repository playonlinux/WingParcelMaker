import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Model } from '../../interfaces';

export class Item extends Model {
  @ApiProperty({ type: String })
  @IsString()
  @IsNotEmpty()
  id: string;

  @ApiProperty({ type: String })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ type: Number })
  @IsNumber()
  @IsNotEmpty()
  weight: number;

  constructor(id: string, name: string, weight: number) {
    super();
    this.id = id;
    this.name = name;
    this.weight = weight;
  }
}
