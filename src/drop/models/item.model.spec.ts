import { validate } from "class-validator";
import { Item } from "./item.model";

describe('Item', () => {

  describe('should throw when id or name or weight are missing', () => {
    it('when id is missing', async () => {
      const item = new Item(undefined, 'name', 3);
      const errors = await validate(item);
      expect(errors.length).not.toBe(0);
    });
    it('when name is missing', async () => {
      const item = new Item('itemId', undefined, 3);
      const errors = await validate(item);
      expect(errors.length).not.toBe(0);
    });    
    it('when weight is missing', async () => {
      const item = new Item('itemId', 'name', undefined);
      const errors = await validate(item);
      expect(errors.length).not.toBe(0);
    });
  });
});

