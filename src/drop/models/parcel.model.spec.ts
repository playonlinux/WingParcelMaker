import { validate } from "class-validator";
import { OrderItem } from "./order-item.model";
import { Parcel } from "./parcel.model";

describe('Parcel', () => {

  describe('should throw when orderId or items array or weight fields are missing', () => {
    it('when orderId is missing', async () => {
      const parcel = new Parcel(undefined, [new OrderItem('iu', 4)], 9);
      const errors = await validate(parcel);
      expect(errors.length).not.toBe(0);
    });
    it('when items array is missing', async () => {
      const parcel = new Parcel('orderId', undefined, 5);
      const errors = await validate(parcel);
      expect(errors.length).not.toBe(0);
    });
    it('when items array is empty', async () => {
      const parcel = new Parcel('orderId', [], 5);
      const errors = await validate(parcel);
      expect(errors.length).not.toBe(0);
    });
    it('when weight is missing', async () => {
      const parcel = new Parcel('orderId', [new OrderItem('iu', 4)], undefined);
      const errors = await validate(parcel);
      expect(errors.length).not.toBe(0);
    });
  });
});

