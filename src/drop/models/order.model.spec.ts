import { validate } from "class-validator";
import { OrderItem } from "./order-item.model";
import { Order } from "./order.model";

describe('Order', () => {

  describe('should throw when id or items array or date are missing', () => {
    it('when id is missing', async () => {
      const order = new Order(undefined, [new OrderItem('iu', 4)], Date.UTC(Date.now()).toString());
      const errors = await validate(order);
      expect(errors.length).not.toBe(0);
    });
    it('when items array is missing', async () => {
      const order = new Order('orderId', undefined, Date.UTC(Date.now()).toString());
      const errors = await validate(order);
      expect(errors.length).not.toBe(0);
    });
    it('when items array is empty', async () => {
      const order = new Order('orderId', [], Date.UTC(Date.now()).toString());
      const errors = await validate(order);
      expect(errors.length).not.toBe(0);
    });
    it('when date is missing', async () => {
      const order = new Order('orderId', [new OrderItem('iu', 4)], undefined);
      const errors = await validate(order);
      expect(errors.length).not.toBe(0);
    });
  });
});

