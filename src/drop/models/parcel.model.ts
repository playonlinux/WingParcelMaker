import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Model } from '../../interfaces';
import { OrderItem } from './order-item.model';

export class Parcel extends Model {

  @ApiProperty({ type: String })
  @IsString()
  @IsNotEmpty()
  orderId: string;

  @ApiProperty({ type: [OrderItem] })
  @IsArray()
  @ArrayNotEmpty()
  @IsNotEmpty()
  items: Array<OrderItem>;

  @ApiProperty({ type: [Number] })
  @IsNumber()
  @IsNotEmpty()
  weight: number;

  @ApiProperty({ type: [String] })
  @IsString()
  trackingId: string;

  @ApiProperty({ type: [Number] })
  @IsNumber()
  @IsNotEmpty()
  paletteNumber: number;

  constructor(
    orderId: string,
    items: OrderItem[],
    weight: number,
    trackingId: string = '',
    paletteNumber: number = 0
  ) {
    super();
    this.orderId = orderId;
    this.items = items;
    this.weight = weight;
    this.trackingId = trackingId;
    this.paletteNumber = paletteNumber;
  }
}
