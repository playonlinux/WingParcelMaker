import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Model } from '../../interfaces';

export type OrderItemWeight = OrderItem & { weight: number };

export class OrderItem extends Model {
  @ApiProperty({ type: String })
  @IsString()
  @IsNotEmpty()
  itemId: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  quantity: number;

  constructor(itemId: string, quantity: number) {
    super();
    this.itemId = itemId;
    this.quantity = quantity;
  }
}
