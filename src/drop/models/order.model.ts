import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsDateString, IsNotEmpty, IsString } from 'class-validator';
import { Model } from '../../interfaces';
import { OrderItem } from './order-item.model';

export class Order extends Model {
  @ApiProperty({ type: String })
  @IsString()
  @IsNotEmpty()
  id: string;

  @ApiProperty({ type: [OrderItem] })
  @IsArray()
  @ArrayNotEmpty()
  @IsNotEmpty()
  items: OrderItem[];

  @ApiProperty({ type: String })
  @IsDateString()
  @IsNotEmpty()
  date: string;

  constructor(id: string, items: OrderItem[], date: string) {
    super();
    this.id = id;
    this.items = items;
    this.date = date;
  }
}
