import { Injectable } from '@nestjs/common';
import { Parcel } from '../models/parcel.model';


@Injectable()
export class RemunerationService {

  private readonly REMUNERATION_RATES = [
    { maxWeight: 1, rate: 1 },
    { maxWeight: 5, rate: 2 },
    { maxWeight: 10, rate: 3 },
    { maxWeight: 20, rate: 5 },
    { maxWeight: Infinity, rate: 10 },
  ];

  private get remunerationRates() {
    return this.REMUNERATION_RATES.sort((r1, r2) => r1.maxWeight < r2.maxWeight ? -1 : 1);
  }

  computeRemuneration(parcels: Parcel[]): number {
    return parcels.reduce((carry: number, parcel: Parcel) =>
      carry + this.computeParcelRemuneration(parcel.weight)
      , 0);
  }

  private computeParcelRemuneration(weight: number): number {
    const { rate } = this.remunerationRates.find(
      ({ maxWeight }) => weight <= maxWeight,
    );
    return rate;
  }

}
