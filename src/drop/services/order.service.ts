import { Injectable } from "@nestjs/common";
import { Order, OrderItem } from "../models";

import { orders } from '../../infrastructure/database';


@Injectable()
export class OrderService {
  getOrders(): Order[] {
    return orders.map(order =>
      new Order(
        order.id,
        order.items.map(item => new OrderItem(item.item_id, item.quantity)),
        order.date
      )
    );
  }
}