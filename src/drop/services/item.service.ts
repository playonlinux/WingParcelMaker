import { Injectable } from "@nestjs/common";
import { Item } from '../models';

import { items } from '../../infrastructure/database';

@Injectable()
export class ItemService {
  getItems(): Item[] {
    return items.map(item => new Item(item.id, item.name, +item.weight));
  }
}

