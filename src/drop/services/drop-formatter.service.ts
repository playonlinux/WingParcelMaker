import { Injectable } from '@nestjs/common';
import { DropResultDto } from '../dto/drop-result.dto';
import { ParcelDto } from '../dto/parcel.dto';
import { IDropFormatter } from '../interfaces';
import { DropResult } from '../models/drop-result.model';


@Injectable()
export class DropFormatterService implements IDropFormatter {
  format(dropResult: DropResult): DropResultDto {
    const dropResultDto = new DropResultDto();
    dropResultDto.parcels = dropResult.parcels.map(parcel => new ParcelDto(parcel));
    dropResultDto.remuneration = dropResult.remuneration;
    return dropResultDto;
  }
}
