import { Injectable } from '@nestjs/common';
import { DTO } from '../../interfaces';
import { IDropPaser } from '../interfaces';

class DropRequestDto extends DTO { }

@Injectable()
export class DropParserService implements IDropPaser {
  async parse(dropRequestDto: DropRequestDto): Promise<any> {
    return null;
  }
}
