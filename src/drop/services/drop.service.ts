import { Injectable } from '@nestjs/common';
import { randomInt } from 'crypto';
import { Item } from '../models/item.model';
import { OrderItemWeight } from '../models/order-item.model';
import { Order } from '../models/order.model';
import { Parcel } from '../models/parcel.model';

const MAX_PARCEL_WEIGHT = 30;
const MAX_PARCEL_NUMBER = 15;

@Injectable()
export class DropService {


  async makeParcels(orders: Order[], items: Item[]): Promise<Parcel[]> {
    let paletteNumber = 0;

    return Promise.all(orders.reduce((parcels: Parcel[], order: Order) => ([
      ...parcels,
      ...this.splitIntoParcels(order, items)
    ]), []).map( (parcel: Parcel, parcelIndex: number) => ({
      ...parcel,
      trackingId: this.generateTrackingCode(),
      paletteNumber: (parcelIndex % MAX_PARCEL_NUMBER === 0) ? ++paletteNumber : paletteNumber
    })));
  }

  private splitIntoParcels = (order: Order, items: Item[]): Parcel[] => {
    const orderItems: OrderItemWeight[] = order.items.map((orderItem) => ({
      itemId: items.find(item => orderItem.itemId === item.id).id,
      weight: +items.find(item => orderItem.itemId === item.id).weight,
      quantity: orderItem.quantity
    }));

    return orderItems.reduce<Parcel[]>((parcels, orderItem) => {
      let remainingQuantity = orderItem.quantity;
      while (remainingQuantity > 0) {
        const quantityToAdd = Math.min(remainingQuantity, Math.floor(MAX_PARCEL_WEIGHT / orderItem.weight));
        const orderItemWeight = quantityToAdd * orderItem.weight;

        let added = false;
        for (const parcel of parcels) {
          if (parcel.weight + orderItemWeight <= MAX_PARCEL_WEIGHT) {
            parcel.items.push({ itemId: orderItem.itemId, quantity: quantityToAdd });
            parcel.weight += orderItemWeight;
            remainingQuantity -= quantityToAdd;
            added = true;
            break;
          }
        }

        if (!added) {
          parcels.push(
            new Parcel(order.id,[{ itemId: orderItem.itemId, quantity: quantityToAdd }], orderItemWeight)
          );
          remainingQuantity -= quantityToAdd;
        }
      }

      return parcels;
    }, []);
  }

  // Access denied due to a lot of calls
  // async generateTrackingCode(): Promise<string> {
  //   const response = await fetch(
  //     'https://www.random.org/integers/?num=1&min=100000000&max=110000000&col=1&base=10&format=plain&rnd=new',
  //   );
  //   const data = await response.text();
  //   return data.trim();
  // }

  generateTrackingCode(): string {
    return `${randomInt(100000000, 110000000)}`;
  }



}
