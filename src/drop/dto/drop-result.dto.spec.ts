import { validate } from 'class-validator';
import { DropResultDto } from './drop-result.dto';

describe('DropResultDto', () => {

  it('should throw when bids array and reservePrice fields are both missing', async () => {
    const dropResultDto = new DropResultDto();
    const errors = await validate(dropResultDto);
    expect(errors.length).not.toBe(0);
  });

  // it('should throw when bids array field is missing', async () => {
  //   const dropResultDto = new DropResultDto();
  //   dropResultDto.reservePrice = 100;
  //   const errors = await validate(dropResultDto);
  //   expect(errors.length).not.toBe(0);
  // });

  // it('should throw when reservePrice field is missing', async () => {
  //   const dropResultDto = new DropResultDto();
  //   dropResultDto.bids = [];
  //   const errors = await validate(dropResultDto);
  //   expect(errors.length).not.toBe(0);
  // });

  // it('should not throw when bids and reservePrice field are filled', async () => {
  //   const dropResultDto = new DropResultDto();
  //   dropResultDto.bids = [new AuctionBidDto('A', [200])];
  //   dropResultDto.reservePrice = 100;
  //   const errors = await validate(dropResultDto);
  //   expect(errors.length).toBe(0);
  // });
});

