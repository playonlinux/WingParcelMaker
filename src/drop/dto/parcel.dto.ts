import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { DTO } from '../../interfaces';
import { Parcel } from '../models';
import { OrderItemDto } from './order-item.dto';


export class ParcelDto extends DTO {

  @ApiProperty({ required: true, type: String })
  @IsString()
  @IsNotEmpty()
  order_id: string;

  @ApiProperty({ required: true, type: [OrderItemDto] })
  @IsArray()
  @ArrayNotEmpty()
  @IsNotEmpty()
  items: OrderItemDto[];

  @ApiProperty({ required: true, type: Number })
  @IsNumber()
  @IsNotEmpty()
  weight: number;

  @ApiProperty({ required: true, type: String })
  @IsString()
  @IsNotEmpty()
  tracking_id: string;

  @ApiProperty({ required: true, type: Number })
  @IsNumber()
  @IsNotEmpty()
  palette_number: number;

  constructor(parcel: Parcel) {
    super();
    this.order_id = parcel.orderId;
    this.items = parcel.items.map(item => new OrderItemDto(item.itemId, item.quantity));
    this.weight = parcel.weight;
    this.tracking_id = parcel.trackingId;
    this.palette_number = parcel.paletteNumber;
  }
}