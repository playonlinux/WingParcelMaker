import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { DTO } from '../../interfaces';

export class OrderItemDto extends DTO {
  @ApiProperty({ type: String })
  @IsNotEmpty()
  item_id: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  quantity: number;

  constructor(itemId: string, quantity: number) {
    super();
    this.item_id = itemId;
    this.quantity = quantity;
  }
}
