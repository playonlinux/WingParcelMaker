import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsNotEmpty, IsNumber } from 'class-validator';
import { DTO } from '../../interfaces';
import { ParcelDto } from './parcel.dto';

export class DropResultDto extends DTO {
  @ApiProperty({ required: true, type: [ParcelDto] })
  @IsArray()
  @ArrayNotEmpty()
  @IsNotEmpty()
  parcels: ParcelDto[];

  @ApiProperty({ required: true, type: Number })
  @IsNumber()
  @IsNotEmpty()
  remuneration: number;
}
