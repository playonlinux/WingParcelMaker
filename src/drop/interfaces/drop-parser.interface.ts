import { IParser } from "src/interfaces";
import { DropRequestDto } from "../dto/drop-request.dto";

export interface IDropPaser extends IParser<DropRequestDto, any> { }
