import { IFormatter } from "../../interfaces";
import { DropResultDto } from "../dto";
import { DropResult } from "../models/drop-result.model";

export interface IDropFormatter extends IFormatter<DropResult, DropResultDto> { }