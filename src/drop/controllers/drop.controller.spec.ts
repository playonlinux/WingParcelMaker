import { Test, TestingModule } from '@nestjs/testing';
import { IFORMATTER_TOKEN, IPARSER_TOKEN } from '../../interfaces';
import { DropResultDto, ParcelDto } from '../dto';
import { Item, Order, OrderItem, Parcel } from '../models';
import { DropFormatterService, DropParserService, DropService, ItemService, OrderService } from '../services';
import { RemunerationService } from '../services/remuneration.service';
import { DropController } from './drop.controller';


describe('AppController', () => {
  let dropController: DropController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [DropController],
      providers: [
        DropService,
        OrderService,
        ItemService,
        RemunerationService,
        {
          provide: IPARSER_TOKEN,
          useClass: DropParserService,
        },
        {
          provide: IFORMATTER_TOKEN,
          useClass: DropFormatterService,
        }
      ],
    }).compile();

    dropController = app.get<DropController>(DropController);
  });

  describe('DropController', () => {

    beforeAll(() => {
      const generator = generateTrackingCode();
      jest.spyOn(OrderService.prototype, 'getOrders').mockReturnValue(orders)
      jest.spyOn(ItemService.prototype, 'getItems').mockReturnValue(items);
      jest.spyOn(DropService.prototype, 'generateTrackingCode').mockImplementation(() => generator.next().value)
    });

    afterAll(() => {
      jest.restoreAllMocks();
    });
    it('should be defined', () => {
      expect(dropController).toBeDefined();
    });

    it('should return drop parcels and remuneration', async () => {
      const result = new DropResultDto();
      result.parcels = parcels;
      result.remuneration = 50;
      expect(await dropController.makeDrop()).toStrictEqual(result);
    });
  });
});


function* generateTrackingCode(): Generator<string, string, string> {
  let trackingId = 100000000;
  while(true) {
    yield `${trackingId++}`;
  }
}


const orders = [
  new Order(
    '5bb61dfdf343a51e8519467e',
    [
      new OrderItem('5bb619e4ebdccb9218aa9dcb', 4),
      new OrderItem('5bb619e44251009d72e458b9', 1),
      new OrderItem('5bb619e4ebdccb9218aa9dcb', 3),
      new OrderItem('5bb619e49593e5d3cbaa0b52', 3),
      new OrderItem('5bb619e4504f248e1be543d3', 4),
      new OrderItem('5bb619e4ebdccb9218aa9dcb', 3)
    ],
    'Tue Oct 02 2018 09:33:28 GMT+0000 (UTC)'
  )
];

const items = [
  new Item('5bb619e49593e5d3cbaa0b52', 'Flowers', 1.5),
  new Item('5bb619e4ebdccb9218aa9dcb', 'Chair', 8.4),
  new Item('5bb619e4911037797edae511', 'TV', 20.8),
  new Item('5bb619e4504f248e1be543d3', 'Skateboard', 5.9),
  new Item('5bb619e40fee29e3aaf09759', 'Donald Trump statue', 18.4),
  new Item('5bb619e44251009d72e458b9', 'Molkkÿ game', 17.9),
  new Item('5bb619e439d3e99e2e25848d', 'Helmet', 22.7)
];

const parcels = [
  new ParcelDto(
    new Parcel('5bb61dfdf343a51e8519467e',
      [
        new OrderItem('5bb619e4ebdccb9218aa9dcb', 3),
        new OrderItem('5bb619e49593e5d3cbaa0b52', 3)
      ],
      29.700000000000003,
      '100000000',
      1
    )
  ),
  new ParcelDto(
    new Parcel('5bb61dfdf343a51e8519467e',
      [
        new OrderItem('5bb619e4ebdccb9218aa9dcb', 1),
        new OrderItem('5bb619e44251009d72e458b9', 1)
      ],
      26.299999999999997,
      '100000001',
      1
    )
  ),
  new ParcelDto(
    new Parcel('5bb61dfdf343a51e8519467e',
      [
        new OrderItem('5bb619e4ebdccb9218aa9dcb', 3)
      ],
      25.200000000000003,
      '100000002',
      1
    )),
  new ParcelDto(
    new Parcel('5bb61dfdf343a51e8519467e',
      [
        new OrderItem('5bb619e4504f248e1be543d3', 4)
      ],
      23.6,
      '100000003',
      1
    )
  ),
  new ParcelDto(
    new Parcel('5bb61dfdf343a51e8519467e',
      [
        new OrderItem('5bb619e4ebdccb9218aa9dcb', 3)
      ],
      25.200000000000003,
      '100000004',
      1
    )
  )
];
