import {
  Controller,
  Get,
  HttpCode,
  Inject
} from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import {
  IFORMATTER_TOKEN
} from '../../interfaces';
import { DropResultDto } from '../dto';
import { IDropFormatter } from '../interfaces';
import { DropResult } from '../models/drop-result.model';
import { Parcel } from '../models/parcel.model';
import { DropService, ItemService, OrderService } from '../services';
import { RemunerationService } from '../services/remuneration.service';

@ApiTags('Drop')
@Controller('drop')
export class DropController {
  constructor(
    // @Inject(IPARSER_TOKEN)
    // private readonly dropParser: IDropPaser,
    private readonly dropService: DropService,
    private readonly orderService: OrderService,
    private readonly itemService: ItemService,
    private readonly dropRemunerationService: RemunerationService,
    @Inject(IFORMATTER_TOKEN)
    private readonly dropFormatter: IDropFormatter
  ) { }


  @Get('/make')
  @HttpCode(200)
  @ApiOkResponse({
    type: DropResultDto,
    description: 'The parcel list with remuneration amount',
  })
  async makeDrop(): Promise<DropResultDto> {
    const result = await this.process();
    return this.dropFormatter.format(result);
  }

  private async process(): Promise<DropResult> {
    const orders = this.orderService.getOrders();
    const items = this.itemService.getItems();

    const parcels: Parcel[] = await this.dropService.makeParcels(orders, items);
    const remuneration = this.dropRemunerationService.computeRemuneration(parcels);

    return new DropResult(parcels, remuneration);
  }
}


