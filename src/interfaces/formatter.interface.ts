import { DTO } from './dto.interface';
import { Model } from './model.interface';

export interface IFormatter<T extends Model, U extends DTO> {
  format(input: T): U;
}

export const IFORMATTER_TOKEN = Symbol('IFormatter');
