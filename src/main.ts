import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { DropModule } from './drop/drop.module';

async function bootstrap() {
  const app = await NestFactory.create(DropModule);

  const config = new DocumentBuilder()
    .setTitle('Drop')
    .setDescription('')
    .setVersion('1.0')
    .addTag('Drop')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);
}
bootstrap();
