'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">wing-parcel-maker documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/DropModule.html" data-type="entity-link" >DropModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-DropModule-6e94127be6a1621d22473a41705554d7121c54a498cbdcbfe7ca6aed239accdbcf0527ca42dc0aadb712242a24baf4349a1ddddb6d5c2d42c0edf90106e28473"' : 'data-bs-target="#xs-controllers-links-module-DropModule-6e94127be6a1621d22473a41705554d7121c54a498cbdcbfe7ca6aed239accdbcf0527ca42dc0aadb712242a24baf4349a1ddddb6d5c2d42c0edf90106e28473"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-DropModule-6e94127be6a1621d22473a41705554d7121c54a498cbdcbfe7ca6aed239accdbcf0527ca42dc0aadb712242a24baf4349a1ddddb6d5c2d42c0edf90106e28473"' :
                                            'id="xs-controllers-links-module-DropModule-6e94127be6a1621d22473a41705554d7121c54a498cbdcbfe7ca6aed239accdbcf0527ca42dc0aadb712242a24baf4349a1ddddb6d5c2d42c0edf90106e28473"' }>
                                            <li class="link">
                                                <a href="controllers/DropController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DropController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-DropModule-6e94127be6a1621d22473a41705554d7121c54a498cbdcbfe7ca6aed239accdbcf0527ca42dc0aadb712242a24baf4349a1ddddb6d5c2d42c0edf90106e28473"' : 'data-bs-target="#xs-injectables-links-module-DropModule-6e94127be6a1621d22473a41705554d7121c54a498cbdcbfe7ca6aed239accdbcf0527ca42dc0aadb712242a24baf4349a1ddddb6d5c2d42c0edf90106e28473"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-DropModule-6e94127be6a1621d22473a41705554d7121c54a498cbdcbfe7ca6aed239accdbcf0527ca42dc0aadb712242a24baf4349a1ddddb6d5c2d42c0edf90106e28473"' :
                                        'id="xs-injectables-links-module-DropModule-6e94127be6a1621d22473a41705554d7121c54a498cbdcbfe7ca6aed239accdbcf0527ca42dc0aadb712242a24baf4349a1ddddb6d5c2d42c0edf90106e28473"' }>
                                        <li class="link">
                                            <a href="injectables/DropService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DropService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ItemService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ItemService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/OrderService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OrderService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/RemunerationService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RemunerationService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#controllers-links"' :
                                'data-bs-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/DropController.html" data-type="entity-link" >DropController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/DropRequestDto.html" data-type="entity-link" >DropRequestDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/DropRequestDto-1.html" data-type="entity-link" >DropRequestDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/DropResult.html" data-type="entity-link" >DropResult</a>
                            </li>
                            <li class="link">
                                <a href="classes/DropResultDto.html" data-type="entity-link" >DropResultDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/DTO.html" data-type="entity-link" >DTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/Item.html" data-type="entity-link" >Item</a>
                            </li>
                            <li class="link">
                                <a href="classes/Model.html" data-type="entity-link" >Model</a>
                            </li>
                            <li class="link">
                                <a href="classes/Order.html" data-type="entity-link" >Order</a>
                            </li>
                            <li class="link">
                                <a href="classes/OrderItem.html" data-type="entity-link" >OrderItem</a>
                            </li>
                            <li class="link">
                                <a href="classes/OrderItemDto.html" data-type="entity-link" >OrderItemDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/Parcel.html" data-type="entity-link" >Parcel</a>
                            </li>
                            <li class="link">
                                <a href="classes/ParcelDto.html" data-type="entity-link" >ParcelDto</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#injectables-links"' :
                                'data-bs-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/DropFormatterService.html" data-type="entity-link" >DropFormatterService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DropParserService.html" data-type="entity-link" >DropParserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DropService.html" data-type="entity-link" >DropService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ItemService.html" data-type="entity-link" >ItemService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OrderService.html" data-type="entity-link" >OrderService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#interfaces-links"' :
                            'data-bs-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/IDropFormatter.html" data-type="entity-link" >IDropFormatter</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IDropPaser.html" data-type="entity-link" >IDropPaser</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IFormatter.html" data-type="entity-link" >IFormatter</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IParser.html" data-type="entity-link" >IParser</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});