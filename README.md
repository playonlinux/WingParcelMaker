## Description

Parcel maker

## Installation

```bash
$ npm install
```

## Running the app

```bash
# build
$ npm run build

# development
$ npm run start

# watch mode
$ npm run start:dev

# watch and debug mode
$ npm run start:debug

# production mode
$ npm run start:prod

# open browser for swagger : http://127.0.0.1:3000/api 
# use Postman with URL http://127.0.0.1:3000/
```

## Test

```bash
# unit tests
$ npm run test

# unit tests watch mode
$ npm run test:watch

# unit tests debug mode
$ npm run test:debug

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Quality

```bash
# format
$ npm run format

# lint
$ npm run lint
```

## Documentation

```bash
# generate doc
$ npm run doc

# generate doc watch mode
$ npm run doc:watch

# open browser for documentation  at http://127.0.0.1:8080
```

